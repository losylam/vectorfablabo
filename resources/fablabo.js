/*
  ##################################################################################################    
  ##              V    E    C    T    O    R    F    A    B    L    A    B    O                   ##
  ##################################################################################################
  ##                           MEDIAWIKI SKIN USING DEFAULT VECTOR SKIN                           ##
  ##################################################################################################
  ##                                                                                              ##
  ##  Développé pour le site : FABLABO.NET (http://fablabo.net)                                   ##
  ##  par :                    Gaëtan CIEPLICKI (https://github.com/divag)                        ##
  ##  pour :                   PiNG (http://wwww.pingbase.net)                                    ##
  ##                                                                                              ##
  ##################################################################################################
  ##                                                                                              ##
  ##  Scripts de la skin vectorfablabo, skin mediawiki qui étend la skin Vector.                  ##
  ##                                                                                              ##
  ##################################################################################################
*/

$( document ).ready(function() {

    //***********************************
    // Sommaire
    //***********************************

    // var top_sommaire = 65;

    // function place_toc(){
    // 	if ($( window ).width() < 1200){
    // 	    $('#sommaire').css('left', '10px');
    // 	    $('#sommaire').css('width', '200px');
    // 	    top_sommaire = $('#mw-panel').height();
    // 	    $('#sommaire').css('top', top_sommaire+'px');
    // 	    console.log(top_sommaire);
    // 	}	
    // }

    // place_toc();
    // // sommaire et largeur de page


    // // sommaire et scroll
    // $( window ).scroll(function() {
    // 	var t = $( window ).scrollTop();
    // 	if (t < top_sommaire){
    // 	    $("#sommaire").css( "top", (top_sommaire-t) +'px' );
    // 	}else{
    // 	    $("#sommaire").css( "top", "1em" );
    // 	}
    // });

    //***********************************
    // Menu top bar
    //***********************************


    // menu personnel
    if (mw.config.get('wgUserId')){
	      console.log('user');
	      $('#pt-userpage').appendTo('#vf-personal-button');

	      $('#p-personal').css('width',45+$('#pt-userpage').width()+'px');

	      $("#pt-userpage").replaceWith(function() {
	          return $("<div>",{'id':"vf-pt-userpage",
				                      html:this.innerHTML
			                       });
	      });
    }

    // menu edition    
    if (($('#right-navigation ul li').length>0) && (!$('body').hasClass('rootpage-Accueil'))){
	      $('#right-navigation').css('display', 'none');
	      $('#left-navigation').css('display', 'none');
	      
	      $("<div>", {'id':"vf-edition-menu",
		                'class':"vf-menu-topbar"}).appendTo("#mw-head");
	      
	      $("<div>", {'id':"vf-edition-button",
		                'class':"vf-menu-topbar-button"}).appendTo("#vf-edition-menu");
	      
	      if (mw.config.get('wgAction').indexOf('edit')>=0){
	          $("#vf-edition-button").addClass('vf-edit-mode');
	      }else{
	          $("#vf-edition-button").addClass('vf-nonedit-mode');
	      }

	      $("<div>", {'id':"vf-edition-list",
		                'class':"vf-menu-topbar-list"}).appendTo("#vf-edition-menu");
	      
	      if ($('#ca-watch').length){
	          console.log($('#ca-watch'));
	          $("<a>", {'id':"vf-watch-menu",
		                  'class':"vf-menu-topbar-button vf-watch",
		                  'href':$('#ca-watch a').first()[0].href,
		                  'title':'suivre la page'
		                 }).appendTo("#mw-head");
	      }

	      if ($('#ca-unwatch').length){
	          console.log($('#ca-unwatch'));
	          $("<a>", {'id':"vf-watch-menu",
		                  'class':"vf-menu-topbar-button vf-unwatch",
		                  'href':$('#ca-unwatch a').first()[0].href,
		                  'title':'ne plus suivre la page'
		                 }).appendTo("#mw-head");
	      }

	      $("#right-navigation ul li").each(function(){
	          $("<div>", {'class':'vf-menu-topbar-item',
			                  'html':this.innerHTML}
	           ).appendTo('#vf-edition-list');
	      });
    }
    
    //***********************************
    // Gestion des vignettes de projet :
    //***********************************

    // largeur auto quand la page est une page fichier
    if (mw.config.get('wgCanonicalNamespace') === 'File'){
	      $('#content').css('max-width', 'none');
	      $('#content').css('width', 'auto');
    };
    
    $('#mw-panel').append($('.srf-tagcloud-sphere'));

    // $('.srf-tagcloud a').each(function(){
    // 	$(this).html($(this).html().replace('Catégorie:', ''));
    // });


    // barre edition
    $('#p-namespaces ul li').last().find('span').css('padding-right', '0');
    $('#p-views ul li').last().find('span').css('padding-right', '0');
    
    // redimenssionement des images // a discuter
    var w_max = $('#content').width() 
    $('.thumbinner').each(function(){
	      if ($(this).width() > w_max){
	          $(this).attr('style', "width:"+ w_max + 'px');
	          var cur_img = $('img', this);
	          cur_width = cur_img.width()
	          cur_height = cur_img.height();
	          cur_img.attr('width', (w_max-4) + "px");
	          cur_img.attr('height', (w_max-4)*cur_height/cur_width + "px");

	      }
    });

    // redimensionnement des vignettes
    $('.vf-vignette').each(function() {
	      var currentElement = $(this);
	      var vignWidth = currentElement.width();
	      var vignHeight = currentElement.height();
	      var imgElement = currentElement.find('img');
	      var aElement = currentElement.find('a');
	      var imgHeight = imgElement.height();
	      var imgWidth = imgElement.width();
	      if (imgHeight < vignHeight){
	          var w = imgWidth * vignHeight/imgHeight;
	          imgElement.attr('height', vignHeight+'px');
	          imgElement.attr('width', w+'px');
	          imgElement.css('left', -(w - vignWidth)/2+'px');
	      }
    });

    $('.vf-vignette').last().next().css('clear', 'both');

    /// en rouge les champs de formulaire pas remplis
    if (mw.config.get('wgAction') == 'formedit'){
	      $('.vf-form-entry-input input').each(function(){
	          if ($(this).val() == ''){
		            $(this).addClass('vf-empty-entry');
	          }
	      });
	      $('.vf-form-entry-input input').keyup(function(){
	          if ($(this).val() == ''){
		            $(this).addClass('vf-empty-entry');
	          }else{
		            $(this).removeClass('vf-empty-entry');
	          }
	      });
    }
    
    // suppression des éléments de l'info box vide (a discuter)
    $('.vf-cartouche-infos').each(function(){
	      if(!$.trim($('.vf-cartouche-infos-donnee', this).html())){
	          $(this).css('display', 'none');
	      }
    });

    var cpt = 1;
    $('.vf-etape').find('h2').each(function(){
	      var sp = $(this).find('span.mw-headline');
	      sp.html("Étape " + cpt + " : " + sp.html());
	      cpt += 1;
    });

    //***********************************
    // Gallerie :
    //***********************************

    var get_fullres_src = function(elem){
	      var img_src = elem.attributes.src.value;
	      img_src = img_src.replace('/thumb', '');
	      img_src = img_src.split('/').slice(0, -1).join('/');
	      return img_src;
    }

    var display_buttons = function(elem){
	      if (elem.parents('.gallerybox').is(':first-child')){
	          $('.vf-button-before').css('display','none');
	      }else{
	          $('.vf-button-before').css('display','block');
	      }

	      if (elem.parents('.gallerybox').is(':last-child')){
	          $('.vf-button-after').css('display','none');
	      }else{
	          $('.vf-button-after').css('display','block');
	      }
    }

    var update_caption = function(elem){
	      var caption = $(elem.parents('.gallerybox').find('.gallerytext')[0]).text();
	      $('.vf-fullscreen-caption').text(caption);
    }

    $('.gallery a').off('click');

    $('.gallery a').click(function(e){
	      var cur_elem = $(this);
	      e.preventDefault();
	      var img_src = get_fullres_src($(this).find('img')[0])
	      var img_div = $('<div/>', {class: 'vf-fullscreen-img'}).appendTo('body');
	      $('<img/>', {src: img_src}).appendTo(img_div);
	      $('<div/>', {class: 'vf-fullscreen-caption'}).appendTo(img_div);
	      but_before = $('<div/>', {class: 'vf-button-before'});
	      but_before.appendTo(img_div);
	      but_before.click(function(e){
	          if (!e) var e = window.event;
	          e.cancelBubble = true;
	          if (e.stopPropagation) e.stopPropagation();
	          var next_gallerybox = cur_elem.parents('.gallerybox').prev();
	          cur_elem = $(next_gallerybox.find('a')[0]);
	          display_buttons(cur_elem);
	          var next_src = get_fullres_src(cur_elem.find('img')[0])
	          $('.vf-fullscreen-img img').attr('src', next_src);
	          update_caption(cur_elem);
	      });

	      but_after = $('<div/>', {class: 'vf-button-after'});
	      but_after.appendTo(img_div);
	      but_after.click(function(e){
	          if (!e) var e = window.event;
	          e.cancelBubble = true;
	          if (e.stopPropagation) e.stopPropagation();
	          var next_gallerybox = cur_elem.parents('.gallerybox').next();
	          cur_elem = $(next_gallerybox.find('a')[0]);
	          display_buttons(cur_elem);
	          var next_src = get_fullres_src(cur_elem.find('img')[0])
	          $('.vf-fullscreen-img img').attr('src', next_src);
	          update_caption(cur_elem);
	      });
	      update_caption(cur_elem);
	      display_buttons(cur_elem);
	      
	      img_div.click(function(){
	          $(this).remove();
	      });

    })

    //***********************************
    // Page machine :
    //***********************************
    if (mw.config.get('wgCategories').indexOf('CNC') >= 0){
	      var danger = {'pas':'#4fb337',
		                  'peu':'#8b942a',
		                  'assez':'#cf8714',
		                  'très':'#cf1414'};
	      for (var d in danger){
	          if ($('#vf-cartouche').hasClass('vf-danger-'+d)){
		            $('#vf-tampon').css('color', danger[d]);
	          }
	      }
    }
	  
});

/*
  ##################################################################################################
*/
