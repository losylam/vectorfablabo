Skin Mediawiki pour fablabo.net
==== 

Skin MediaWiki développé par Laurent MALYS
(http://www.laurent-malys.fr) pour l'association PiNG
(http://www.pingbase.net) à partir du thème VectorFablabo développé
par Gaëtan CIEPLICKI (https://github.com/divag)

Structure
----

Le déopt contient, en plus des fichiers qui constitue le thème
VectorFablabo (fichier css + js) : 

- VectorFablabo.LocalSettings.php : contient les configurations à
ajouter à LocalSettings.php 

- pages/*.md : contient les sources aux format markdown de toutes les
pages modifiées, 

  - notamment : 
    - les modèles 
    - les formulaires

  - Pour les pages :

     - Utilisateur (laborantin)
     - Projet
     - CNC

  - Ainsi que les pages
     - Accueil
     - Sidebar

CSS
----

Le thème principal est défini dans le fichier fablabo.less.

Le fichier fablabo-layout.css n'est utilisé que pour la version
desktop.

Pour intégrer fablabo.less à la version mobile, il faut la convertir
en css (avec par exemple l'utilitaire lessc) et copier le contenu du
fichier dans la page MediaWiki:Mobile.css