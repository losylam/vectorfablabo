<noinclude>
Ceci est le formulaire « Projet ».
Pour créer une page avec ce formulaire, entrez le nom de la page ci-dessous ;
si une page avec ce nom existe déjà, vous serez dirigé vers un formulaire pour l’éditer.

{{#forminput:form=Projet}}

</noinclude><includeonly>
<div id="wikiPreview" style="display: none; padding-bottom: 25px; margin-bottom: 25px; border-bottom: 1px solid #AAAAAA;"></div>
{{{for template|Projet}}}

<div class="vf-form-entry">
<div class="vf-form-entry-title">
Status du projet
</div>
<div class="vf-form-entry-description">
(Un choix)
</div>
<div class="vf-form-entry-input">
{{{field|status|input type=listbox|values=Concept,Experimental,Prototype,Fonctionnel,Inconnu,Abandonné,Obsolete}}}
</div>
</div>

<div class="vf-form-entry">
<div class="vf-form-entry-title">
Status de la publication
</div>
<div class="vf-form-entry-description">
(Un choix)
</div>
<div class="vf-form-entry-input">
{{{field|status_pub|input type=listbox|values=Brouillon,Publié,Finalisé}}}
</div>
</div>


<div class="vf-form-entry">
<div class="vf-form-entry-title">
Image
</div>
<div id="vf-form-entry-input-image" class="vf-form-entry-input">
{{{field|image|input type=text with autocomplete|property=image|uploadable|values from namespace=File}}}
</div>
</div>

<div class="vf-form-entry">
<div class="vf-form-entry-title">
Description
</div>
<div class="vf-form-entry-description">
(petite description du projet, 80 caractères max)
</div>
<div class="vf-form-entry-input">
{{{field|description|maxlength=80}}}
</div>
</div>

<div class="vf-form-entry">
<div class="vf-form-entry-title">
License:
</div>
<div class="vf-form-entry-input">
{{{field|license|input type=menuselect|structure={{MediaWiki:Licenses}}|default=CC-by-sa-3.0 - Creative Commons Attribution CC-by-sa-3.0 France}}}
</div>
</div>

<div class="vf-form-entry">
<div class="vf-form-entry-title">
Contributeur(s)
</div>
<div class="vf-form-entry-description">
(personnes participants au projet, séparés par des virgules):
</div>
<div class="vf-form-entry-input">
{{{field|contributeurs|list|values from namespace=User}}}
</div>
</div>

<div class="vf-form-entry">
<div class="vf-form-entry-title">
Inspiration:
</div>
<div class="vf-form-entry-input">
{{{field|inspiration|input type=text}}}
</div>
</div>

<div class="vf-form-entry">
<div class="vf-form-entry-title">
Ingrédients
</div>
<div class="vf-form-entry-description">
(liste des matériaux utilisés, séparés par des virgules)
</div>
<div class="vf-form-entry-input">
{{{field|ingrédients|list|values from category=Materiaux}}}
</div>
</div>

<div class="vf-form-entry">
<div class="vf-form-entry-title">
Outils
</div>
<div class="vf-form-entry-description">
(liste des machines specifiques utilisées, séparés par des virgules)
</div>
<div class="vf-form-entry-input">
{{{field|machines|list|values from category=Machines}}}
</div>
</div>

<div class="vf-form-entry">
<div class="vf-form-entry-title">
Fichier(s) source:
</div>
<div class="vf-form-entry-input">
{{{field|source|input type=text with autocomplete|uploadable|values from namespace=File}}}
</div>
</div>

<div class="vf-form-entry">
<div class="vf-form-entry-title">
Url:
</div>
<div class="vf-form-entry-input">
{{{field|url}}}
</div>
</div>

{{{end template}}}
{{DebugDatafab}}

'''Texte libre:'''

{{{standard input|free text|rows=10|editor=wikieditor}}}


{{{standard input|summary}}}

{{{standard input|minor edit}}} {{{standard input|watch}}}

{{{standard input|save}}} {{{standard input|preview}}} {{{standard input|changes}}} {{{standard input|cancel}}}
</includeonly>

<noinclude>[[category:datafab]]</noinclude>
