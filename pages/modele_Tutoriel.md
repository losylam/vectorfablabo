<noinclude>
Ceci est le modèle « Tutoriel ». Il devrait être appelé selon le format suivant :
{{#forminput:form=tutoriel}}
<pre>
{{Tutoriel
|Description=
|compétences requises=
|étapes=
}}
</pre>
Modifier la page pour voir le texte du modèle.
</noinclude>
<includeonly>
[[Catégorie:Tutoriels]]
<div id="vf-cartouche">
<div id="infos">

<div id="vf-cartouche-vignette">
{{#ifexist: Image:{{{image}}} |[[Image::Fichier:{{{image}}}| ]][[Image:{{{image}}}|340px]]|[[Image::Fichier:JoliAfficheur.jpg| ]][[Image:JoliAfficheur.jpg |340px]]}}
</div>

<div id="vf-cartouche-table">

<div class="vf-cartouche-infos">
<div class="vf-cartouche-infos-titre">
Contributeur(s)
</div>  
<div class="vf-cartouche-infos-donnee">
{{#arraymap:{{{contributeurs|}}}|,|x|[[contributeur::User:x|x]]|\n}}
</div>
</div>

<div class="vf-cartouche-infos">
<div class="vf-cartouche-infos-titre">
Status de la publication
</div>  
<div class="vf-cartouche-infos-donnee">
{{{status_pub}}}
</div>
</div>

<div class="vf-cartouche-infos">
<div class="vf-cartouche-infos-titre">
License
</div>  
<div class="vf-cartouche-infos-donnee">
{{{license|GPL 2.0}}}
</div>
</div>

<div class="vf-cartouche-infos">
<div class="vf-cartouche-infos-titre">
Compétences requises
</div>  
<div class="vf-cartouche-infos-donnee">
{{#arraymap:{{{compétences requises|}}}|,|x|[[Compétences::x]]}}
</div>
</div>

</div>
</div>
</div>

{{#arraymap:{{{étapes|}}}| |x|[[étapes::x]]}}

<div id="sommaire">
__TOC__
</div>
</includeonly>
<noinclude>

<code>
{{Tutoriel
|image=Poste-a-souder-essentiel-medium-4393439.jpg
|description=Souder des composants électroniques
|compétences requises=dextérité, résistance à la chaleur et aux vapeurs toxiques
|contributeurs=Mathilde
|source=apprendre à souder à l'étain et étain/plomb
|Description=Ceci est une description
|quote=Description
}}

Ben c'est la qu'on présente plus en détail à quoi ça sert...
{{Etape
|Titre=Note générale sur la brasure
|Contenu=En fait la soudure à l'étain est une brasure, et non une soudure.
Les pièces à souder doivent être à la température du métal d'apport (ici, l'étain).
Ainsi, l'étain est réellement brasé, et non "posé" (ce qui se passerait sinon, c'est qu'un simple coup d'ongle suffirait à l'enlever!).

Les operatins de soudure presentent deux risques importants au niveau sécurité :

* brûlure
* intoxication ( absorbption des fumées ou produits toxiques)
}}
{{Etape
|Titre=Utilisation et le choix de la gaine thermoretractable
|Contenu=Gaine thermorétractable :

* polyéphine
* gaine "bas de gamme"

Note sur le choix (noter le coeff de retractation).

Note sur l'étanchéité.
 
chainer de la gaine thermo afin de faire un "tatoo" (pour par exemple faire des faisceaux)
}}
{{Etape
|Titre=Quel étain choisir
|Image=Electronic-547671 640.jpg,
|Contenu=En particulier, remarque que les soudures réussies ne sont brillantes que pour l'etain plomb, et non pas la soudure sans plomb.

*étain pur
* etain plomb
* etain argent

températures de fusion
}}
{{Etape
|Titre=Comment dimensionner un fil
|Contenu=A completer :

* pourquoi le fameux barème : 10A -> 1.5 mm2
* loi de Neher-Mc Grath
* utilisation de la loi de pouillet
}}
{{Etape
|Titre=Les contacts électriques
|Contenu=La plupart des incendies électriques sont liés à un contact insuffisant. 
Pensez-y :  quand une boule de petanque touche le sol, c'est en une "infime " partie de sa surface. Il en est de même pour une fiche, ou deux fils lies dans un domino dans le cas où le serrage n'est pas suffisant.

Dans les installations industrielles (ex d'appareil qui consomme beaucoup : un TGV ; les cables des moteurs sont reliés a des cosses, serres au couple)

Voir clef dynamométrique et son utilisation en électronique; dissipateurs ou cosses
}}
</code>

</noinclude>