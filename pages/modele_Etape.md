<noinclude>
Ceci est le modèle « Etape ». Il devrait être appelé selon le format suivant :
<code>
{{Etape
|Titre=Test de la premiere étape
|Image=JoliAfficheur.jpg
|Contenu=Test d'un texte de contenu
assez long

avec des sauts de ligne


et des puces
* genre une puce comme ça
* et une deuxième
}}{{Etape
|Titre=et même d'une deuxième
|Image=
|Contenu=Test d'un texte de contenu
assez long

avec des sauts de ligne


et des puces
* genre une puce comme ça
* et une deuxième
}}
</code>
Modifier la page pour voir le texte du modèle.
</noinclude>
<includeonly>
{{#set_internal:reference|Etape={{{Titre|}}}|Image={{{Image|}}}|Contenu={{{Contenu|}}}}}
<div class="vf-etape"><div class="vf-etape-titre collapsed">
=={{{Titre|}}}==
</div><div class="vf-etape-content">
{{#arraymap:{{{Image|}}}|,|x|[[Image::Fichier:x| ]][[Image:x|frameless|200px]] }}
[[Contenu::{{{Contenu|}}} ]] 
</div></div>
</includeonly>
