<includeonly>
__NOTOC__
  <div id="vf-cartouche">
    <div id="infos">
      <div id="vf-cartouche-vignette">
{{#ifexist: Image:{{{image}}} |[[Image::Fichier:{{{image}}}| ]][[Image:{{{image}}} |left|340px]]|}}
      </div>
      <div id="vf-cartouche-table">

<div class="vf-cartouche-infos">
<div class="vf-cartouche-infos-titre">
Sujets Préférés
</div>  
<div class="vf-cartouche-infos-donnee">
{{#arraymap:{{{Sujets_Preferes|}}}|,|x|[[Sujets_Preferes::x|x]]|\n}}
</div>
</div>

<div class="vf-cartouche-infos">
<div class="vf-cartouche-infos-titre">
Projets
</div>  
<div class="vf-cartouche-infos-donnee">
{{#ask:[[Contributeur::{{FULLPAGENAME}}]]| format=dl }}
</div>
</div>

<div class="vf-cartouche-infos">
<div class="vf-cartouche-infos-titre">
Site perso
</div>  
<div class="vf-cartouche-infos-donnee">
{{{url|}}}
</div>
</div>

<div class="vf-cartouche-infos">
<div class="vf-cartouche-infos-titre">
Compétences
</div>  
<div class="vf-cartouche-infos-donnee">
{{#arraymap:{{{compétences|}}}|,|x|[[compétence::x|x]]|\n}}
</div>
</div>

<div class="vf-cartouche-infos">
<div class="vf-cartouche-infos-titre">
Demande de coup de main
</div>  
<div class="vf-cartouche-infos-donnee">
{{#arraymap:{{{besoins|}}}|,|x|[[besoin::x|x]]|\n}}
</div>
</div>

[[category:Utilisateur]]
</div>
</div>
</div>
  
<br style="clear:both" />

== Les derniers projets de {{PAGENAME}} ==

<div class="vf-liste-vignettes">
{{#ask:[[contributeur::~{{PAGENAME}}]]
 | limit=12
 | sort=Date de modification
 | order=descending
 | ?
 | ?Description
 | ?Image
 | format=template 
 | template=ListeVignettes
 | headers=hide 
 | link=none  
 | offset= }}
</div>

<br style="clear:both" />

</includeonly>

<noinclude>
<div class="MachineBox"   style="width:100%;float: left;">
==Derniers Travaux réalisés par {{PAGENAME}}==
{{#ask:[[Category:Travaux]][[Usager::~{{FULLPAGENAME}}]]
 |?Description
 |?Machine
 |format=broadtable
 |sort=Date de modification
 | order=descending
 |limit=10
 |headers=show
 |link=all
 |class=sortable wikitable smwtable
 |offset=
}}
  </div>
<div class="MachineBox"   style="width:100%;float: right;">
__TOC__

='''Documentation du modèle'''=
Un Boite d'infos pour présenter un contributeur au wiki et un usager de l'atelier
 (encore en cours de développement)

==Paramètres==

*'''image''' (une image téléversée sur ce wiki)
*'''Sujets Préférés''' (une liste de thèmes )
*'''site perso''' (optionnel)
*'''compétences''' ( n'hésitez pas a me solliciter sur ... )
*'''demande de coup de main''' ( l'inverse )

==Usage==

<code>
{{Laborantin
|image=Ced.png
|Sujets_Preferes=Fabrication_Numérique, Velomobile, aviation, Histoire, code
|url=http://cdriko.free.fr
|compétences=tout, kjoyru, iurtir
|besoins=ioezazyfefoiy, iopuezag ipuezag, gfiutgie
}}
</code>

[[Category:Template]]
[[category:datafab]]

  </div>
</noinclude>
