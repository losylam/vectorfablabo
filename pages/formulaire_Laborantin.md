<noinclude>
Ceci est le formulaire « Laborantin ».
Pour créer une page avec ce formulaire, entrez le nom de la page ci-dessous ;
si une page avec ce nom existe déjà, vous serez dirigé vers un formulaire pour l’éditer.


{{#forminput:form=Laborantin}}

</noinclude><includeonly>
<div id="wikiPreview" style="display: none; padding-bottom: 25px; margin-bottom: 25px; border-bottom: 1px solid #AAAAAA;"></div>
{{{for template|Laborantin}}}

<div class="vf-form-entry">
<div class="vf-form-entry-title">
Image
</div>
<div id="vf-form-entry-input-image" class="vf-form-entry-input">
{{{field|image|input type=text with autocomplete|property=image|uploadable|values from namespace=File}}}
</div>
</div>

<div class="vf-form-entry">
<div class="vf-form-entry-title">
Sujets Préférés
</div>
<div class="vf-form-entry-description">
(séparés par une virgule)
</div>
<div class="vf-form-entry-input">
{{{field|Sujets_Preferes|values from category=Thèmes|list}}}
</div>
</div>

<div class="vf-form-entry">
<div class="vf-form-entry-title">
Site Perso
</div>
<div class="vf-form-entry-input">
{{{field|url}}}
</div>
</div>

<div class="vf-form-entry">
<div class="vf-form-entry-title">
Compétences
</div>
<div class="vf-form-entry-description">
(ce que vous connaissez, savez faire...séparés par une virgule)
</div>
<div class="vf-form-entry-input">
{{{field|compétences}}}
</div>
</div>

<div class="vf-form-entry">
<div class="vf-form-entry-title">
Demande de coup de main
</div>
<div class="vf-form-entry-description">
(vous avez des difficultés avec votre projet ? dans un domaine particulier ? séparés par une virgule)
</div>
<div class="vf-form-entry-input">
{{{field|besoins}}}
</div>
</div>

{{{end template}}}

'''Texte libre:'''

{{{standard input|free text|rows=10|editor=wikieditor}}}

{{{standard input|summary}}}

{{{standard input|minor edit}}} {{{standard input|watch}}}

{{{standard input|save}}} {{{standard input|preview}}} {{{standard input|changes}}} {{{standard input|cancel}}}
</includeonly>

<noinclude>[[category:datafab]]</noinclude>
