<includeonly>__NOTOC__<div id="vf-cartouche" class="vf-machine vf-danger-{{{dangerosité}}}">
{{Outil
|nombre={{{nombre}}}
|fonction={{{fonction}}}
|dangerosité={{{dangerosité}}}
}}
<div id="infos">
<div id="vf-cartouche-vignette">
{{#ifexist: Image:{{{image}}} |[[Image::Fichier:{{{image}}}| ]][[Image:{{{image}}} |left|340px]]|}}

</div>
<div id="vf-cartouche-table">

<div class="vf-cartouche-infos">
<div class="vf-cartouche-infos-titre">
Type
</div>  
<div class="vf-cartouche-infos-donnee">
[[type::{{{type|...}}}]]
</div>
</div>

<div class="vf-cartouche-infos">
<div class="vf-cartouche-infos-titre">
Modèle
</div>  
<div class="vf-cartouche-infos-donnee">
{{{modèle|inconnu}}}
</div>
</div>

<div class="vf-cartouche-infos">
<div class="vf-cartouche-infos-titre">
Statut
</div>  
<div class="vf-cartouche-infos-donnee">
[[statut::{{{statut}}}]]
</div>
</div>

<div class="vf-cartouche-infos">
<div class="vf-cartouche-infos-titre">
Située à
</div>  
<div class="vf-cartouche-infos-donnee">
[[localisation::{{{localisation}}}]]
</div>
</div>

</div>
</div>

<div class="vf-cartouche-inline">
Cette machine à commande numérique utilise des fichiers au(x) format(s)<div class="vf-cartouche-infos-donnee-inline">
{{#arraymap:{{{entrées|}}}|,|x|[[entrée::x|x]]|\n}}
</div>
</div>

<div class="vf-cartouche-inline">Elle produit des<div class="vf-cartouche-infos-donnee-inline">
{{#arraymap:{{{sortie|}}}|,|x|[[sortie::x|x]]|\n}}
</div>
</div>

<div class="vf-cartouche-inline">Elle peut travailler les matériaux :<div class="vf-cartouche-infos-donnee-inline">
{{#arraymap:{{{ingredients|}}}|,|x|[[materiau::x|x]]|\n}}
</div>
</div>

<div class="vf-cartouche-inline">
Sa zone de travail est: 
* largeur : '''{{{largeur}}}''' cm
* longueur : '''{{{longueur}}}''' cm
* {{#if: {{{hauteur|}}} |hauteur : '''{{{hauteur}}}''' cm|}}
</div>

</div>

[[category:CNC]]


== Ressources ==

<div class="vf-liens-machine">
<div class="vf-lien-machine">[[{{PAGENAME}}/Modedemploi|Mode d'emploi]]</div>
<div class="vf-lien-machine">[[{{PAGENAME}}/developpement|Mise au point et maintenance]]</div>
</div>
</includeonly>

<noinclude>

__TOC__

='''Documentation du modèle'''=
Un Boite d'infos pour présenter une machine du fablab

==Paramètres==

*'''Image''' (une image téléversée sur ce wiki)
*'''Fonction '''  (sert à ?)
*'''Dangerosité''' (un choix)
*'''Nombre''' (quantité de cet outil)
*'''Modèle''' (modèle de l'outil)
*'''Statut''' (en panne, fonctionnelle...)
*'''Localisation''' (PiNG, PlateformeC, etc)
*'''Entrée''' (format des fichiers sources)
*'''Sortie''' (ce que cette machine produit)
*'''Ingredients''' (materiaux qu'elle utilise)
*'''Largeur''' (de travail en cm)
*'''Longueur''' (de travail en cm)
*''' Hauteur''' (de travail en cm)


==Usage==
<code>
{{CNC
|image=Portrait.jpg
|fonction=tester l'ossature du wiki
|dangerosité=peu
|quantité=1
|modèle=Graftec 2100
|statut=en panne
|localisation=PlateformeC
|entrées=GCode ,?,tt
|sortie=autocollants, affiches, cartons
|ingredients=carton, vinyl, etc
|largeur=36
|longueur=28
|hauteur=24
|function=Tester l'arborescence sémantique du wiki
}}
</code>

[[category:datafab]]
</noinclude>

