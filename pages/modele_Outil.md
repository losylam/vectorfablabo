<includeonly>
<span id="vf-tampon" class="outil">Outil {{{dangerosité}}} dangereux</span>
<div class="vf-cartouche-description">
Cet outil sert à 
</div><div class="vf-cartouche-description"> [[fonction::{{{fonction}}}]]</div>
</includeonly>
<includeonly>[[category:outils]] </includeonly>
<noinclude>

=='''Template Documentation'''==
Les caractéristiques d'un outil utilisé dans le fablab

==Parameters==
===Header===
*'''fonction''' (requis) : "cet outils sert à..."
*'''nombre''' (requis) :" il yen a ..."
:{| class="wikitable"
!dangorosité !!Theme Color
|-
|pas||style="background-color:#CCCCCC"|#CCCCCC
|-
|peu||style="background-color:#FF9933"|#FF9933
|-
|assez||style="background-color:#00B"|<span style="color:white"> #00B </span>
|-
|très||style="background-color:#009200"|<span style="color:white"> #000000 </span>
|}
</noinclude>