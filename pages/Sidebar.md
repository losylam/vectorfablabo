* navigation
** mainpage|mainpage-description
** Projets|Les Projets
** Machines|Les Machines

* SEARCH
* Qui sommes-nous ?
** http://pingbase.net|Le site de PiNG
* Participer à ce Wiki
** Aide:Accueil|Guide d'utilisation
** Formulaire:Projet|Créer un projet
** Formulaire:Tutoriel|Créer un Tutoriel
** Spécial:AjouterDonnées/Tache/{{CURRENTTIMESTAMP}}|Nouvelle Tâche

* TOOLBOX
** recentchanges-url|recentchanges
** ArbreDesCategories|Arbre des catégories
* LANGUAGES
