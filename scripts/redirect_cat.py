#!/env/python
# coding: utf-8

from wikitools import wiki
from wikitools import page

fl_address = "http://fablabo.net/mediawiki-1.27.0/api.php"
username = "LaurentM"
password = "Meaux2Passses"

ch_cat = '/media/www-dev/public/fablabo/mediawiki-1.27.0/skins/VectorFablabo/scripts/categorie_theme'

site = wiki.Wiki(fl_address)
site.login(username, password)

with open(ch_cat, 'r') as f:
    lst_cat = [i.strip() for i in f.readlines() if not i.startswith('#')]

for i in lst_cat:
    p = page.Page(site, title=i)
    if not p.exists:
        p.edit('#REDIRECTION [[Catégorie:%s]]'%i)

