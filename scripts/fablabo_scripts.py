#!/env/python
# coding: utf-8

import marshal
import json

from wikitools import wiki
from wikitools import page

fl_address = "http://192.168.1.122/public/fablabo/mediawiki-1.27.0/api.php"
username = "LaurentM"
password = "Meaux2Passses"

ch_cat = '/media/www-dev/public/fablabo/mediawiki-1.27.0/skins/VectorFablabo/scripts/categorie_theme'
path_db = '/media/www-dev/public/fablabo/mediawiki-1.27.0/skins/VectorFablabo/scripts/data/fablabo_pages'

class WikiMediaStats( ):
    """
    explore the content of a wikimedia site 
    """
    
    def __init__(self, 
                 site_address = fl_address,
                 username = username,
                 password = password,
                 path_db = path_db,
                 init_db = True
             ):
        """
        """
        
        self.site = wiki.Wiki(site_address)
        self.site.login(username, password)

        self.path_db = path_db

        self.pages_by_id = {}
        self.pages = {}
        self.categories = {}
        self.users = {}

        self.nonexistent_page = []



        self.load_db()

    def load_db(self):
        with open(self.path_db, 'r') as f:
            self.pages_by_id = marshal.load(f)

        for i, it in self.pages_by_id.items():
            self.pages[it['title']] = it

    def save_db(self):
        with open(self.path_db, 'w') as f:
            marshal.dump(self.pages_by_id, f)
        
    def import_all_pages(self, n_pages):
        for i in range(1, n_pages):
            if not self.dic_pages.has_key(i) and i not in self.nonexistent_page:
                try:
                    p = page.Page(site, pageid = i)
                    self.dic_pages[i] = {
                        'pageid':i,
                        'title':p.title,
                        'links':p.getLinks(),
                        'template':p.getTemplates(),
                        'wikitext':p.getWikiText(),
                        'history':p.getHistory(),
                        'categories':p.getCategories()
                    }
                except:
                    self.nonexistent_page.append(i)
                    print 'page %s inexistante'% i
        
    def get_categories(self):
        for page in self.pages.values():
            page_categories = [i.split(':')[1] for i in page['categories']]
            for page_category in page_categories:
                if self.categories.has_key(page_category):
                    self.categories[page_category]['pages'].append(page)
                else:
                    self.categories[page_category] = {
                        'pages': [page]
                    }

    def get_page_if_title_start_with(self, startswith):
        # return [i for c, i in self.pages.items() if c.startswith(startswith)]
        output_list = []
        for key, item in self.pages.items():
            if key.startswith(startswith):
                output_list.append(item)
                print(key)
        return output_list
                
    def get_page_if_title_contains(self, contains):
        # return [i for c, i in self.pages.items() if contains in c]
        output_list = []
        for key, item in self.pages.items():
            if contains in key:
                output_list.append(item)
                print(key)
        return output_list

    def export_category_network(self, filename):
        cur_id = 0
        nodes = []
        links = []
        for i in self.pages.values():
            if len(i['categories']) > 0:
                nodes.append({
                    "name":i['title'],
                    "auteurs":[''],
                    "group":0,
                    "id":cur_id,
                    "size":max(1, min(len(i['wikitext'])/500, 10))
                })
                i['network_id'] = cur_id
                cur_id += 1

        for c, i in self.categories.items():
            nodes.append({
                "name":c,
                "group":1,
                "id":cur_id,
                "size":max(1, min(len(i['pages'])/15, 10))
            })
            i['network_id'] = cur_id
            for p in i['pages']:
                links.append({
                    "source": cur_id,
                    "target": p['network_id']
                })
            cur_id += 1

        with open(filename, 'w') as f:
            json.dump({'nodes':nodes, 'links':links}, f)
        
    def get_editions_by_projects(self, pagename = None, pageid = None):
        h = []
        cur_size = 0
        if pagename:
            h = self.pages[pagename]['history']
        elif pageid:
            h = self.pages_by_id[pageid]['history']
            pagename = self.pages_by_id[pageid]['title']
        h.reverse()
        for m in h:
            size_modif = m['size'] - cur_size
            cur_size = m['size']
            if self.users.has_key(m['user']):
                self.users[m['user']]['size'] += size_modif
                if self.users[m['user']]['pages'].has_key(pagename):
                    self.users[m['user']]['pages'][pagename] += size_modif
                else:
                    self.users[m['user']]['pages'][pagename] = size_modif
            else:
                self.users[m['user']] = {}
                self.users[m['user']]['size'] = size_modif
                self.users[m['user']]['pages'] = {pagename:size_modif}

    def get_editions_by_users(self):
        for i, it in self.pages.items():
            
            self.get_editions_by_projects(i)

    def get_editions_by_users_by_projects(self, user):
        l = self.users[user]['pages']
        a = sorted(l, key=l.__getitem__)
        out = []
        for c in a:
            out.append((c, l[c]))
        return out
