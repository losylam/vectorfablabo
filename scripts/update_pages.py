#!/env/python
# coding: utf-8

from wikitools import wiki
from wikitools import page
import os
import subprocess

#fl_address = "http://fablabo.net/mediawiki-1.27.0/api.php"
fl_address = "http://192.168.1.70/public/fablabo/mediawiki-1.27.0-new/api.php"
username = "LaurentM"
password = "Meaux2Passses"

ch_less = '../resources/fablabo.less'
ch_mobile_css = '../resources/fablabo-mobile.css'

ch_pages = '/media/www-dev/public/fablabo/mediawiki-1.27.0/skins/VectorFablabo/pages'

css_replace = 'fonts/'
css_replaceby = 'skins/VectorFablabo/resources/fonts/'

site = wiki.Wiki(fl_address)
site.login(username, password)

lst_pages = os.listdir(ch_pages)

def create_pagenames(lst_p):
    d = {}
    for i in lst_p:
        if i.startswith('modele'):
            pagename = 'Modèle:'+i[:-3].split('_')[1]
        elif i.startswith('categorie'):
            pagename = 'Catégorie:'+i[:-3].split('_')[1]
        elif i.startswith('formulaire'):
            pagename = 'Formulaire:'+i[:-3].split('_')[1]
        elif i.startswith('Sidebar'):
            pagename = "MediaWiki:Sidebar"
        else:
            pagename = i[:-3]
        d[i] = pagename
    return d

d = create_pagenames(lst_pages)

def update_pages(d):
    for i, it in d.items():
        update_page(i, it)

def update_page(i, it):
    with open(os.path.join(ch_pages, i), 'r') as f:
        p = page.Page(site, title=it)
        p.edit(f.read())

# for i in lst_cat:
#     p = page.Page(site, title=i)
#     if not p.exists:
#         p.edit('#REDIRECTION [[Catégorie:%s]]'%i)

def update_css():
    print('process lessc')
    subprocess.call(['lessc', 
                     ch_less,
                     '>', 
                     ch_mobile_css])
    print('update MediaWIki:Mobile.css')
    p = page.Page(site, title='MediaWiki:Mobile.css')
    with open(ch_mobile_css, 'r') as f:
        f = f.read().replace(css_replace, css_replaceby)
        p.edit(f)
    print('DONE updating')
